---
- name: Install Python 3 virtual environment creator
  apt:
    pkg:
      - python3-venv
    state: present

- name: Detect Python 3 executable
  command: which python3
  register: which_python3
  check_mode: false
  changed_when: false

- name: Create CertGet application directory
  file:
    path: /opt/certget
    state: directory

- name: Create virtualenv for CertGet and upgrade pip
  pip:
    name: pip
    extra_args: --upgrade
    virtualenv: /opt/certget/venv
    virtualenv_command: '{{ which_python3.stdout }} -m venv'

- name: Install CertGet
  pip:
    name: git+https://gitlab.com/obda/certget.git
    virtualenv: /opt/certget/venv

- name: Create Let's Encrypt configuration and hook directories
  file:
    path: '{{ item }}'
    state: directory
  loop:
    - /etc/letsencrypt/conf
    - /etc/letsencrypt/renewal-hooks/pre
    - /etc/letsencrypt/renewal-hooks/post

- name: Copy default Let's Encrypt renewal hook scripts
  copy:
    src: '{{ item.hook }}'
    dest: '/etc/letsencrypt/renewal-hooks/{{ item.type }}'
    owner: root
    group: root
    mode: 0755
  loop:
    - hook: create-root
      type: pre
    - hook: remove-root
      type: post
    - hook: restart-nginx
      type: post

- name: Generate certificates and set up cron job
  import_tasks: generate-certs.yml
  when: certget_certificates|length > 0
